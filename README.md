# P7 Project [![Build Status](https://travis-ci.org/DanMeakin/P7-Project.svg?branch=master)](https://travis-ci.org/DanMeakin/P7-Project) [![codecov.io](https://codecov.io/github/DanMeakin/P7-Project/coverage.svg?branch=master)](https://codecov.io/github/DanMeakin/P7-Project?branch=master)

This project is created as a 1. semester project at the It design and application 
development masters program at Aalborg University.

We are designing and implementing (as far as possible within one semester) a
piece of software to provide information to bus passengers on capacity and how
busy particular buses are.

The software is written in Java, and is meant to simulate a mobile application.   